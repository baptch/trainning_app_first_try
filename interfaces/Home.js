import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { View, Image } from 'react-native';
import { Avatar } from 'react-native-elements';
import { Container, Header, Content, Footer, Body, Right, Left, Title, FooterTab, Button, Icon} from 'native-base';
import CustomTableView from '../components/CustomTableView';
import Stages from './Stages';
import Actu from './Actu';
import Presentation from './Presentation';

class Home extends React.Component {
    constructor(props){
        super(props);
        this.toggleView = this.toggleView.bind(this);
        this.state = {
            view: 'Presentation',
        }
    }

    toggleView(view) {
      this.setState({ view });
    }



    render() {

        // let currentView = <Presentation action={() => console.log("pouet")}/>
//On a créer la navigation par bouton il ne reste plus qu'à tester tout ça il faut régler le problème trop chiant et con qui vient du React
// pour le component text. La on a normalement la navigation qui foncitonne
        let currentView = <Presentation navigation={this.props.navigation}/>

        if (this.state.view === 'CustomTableView'){
          currentView = <CustomTableView/>
        }

        else if (this.state.view === 'Stages') {
          currentView = <Stages/>
        }
        else if (this.state.view === 'Actu') {
          currentView = <Actu/>
        }

        return (
          <Container>
              <Content>
                {currentView}
              </Content>
              <Footer>
                  <FooterTab>
                      <Button onPress={() => this.toggleView('Stages')}>
                        <Text>Les Stages</Text>
                      </Button>
                      <Button onPress={() => this.toggleView('CustomTableView')}>
                        <Text>Les Programmes</Text>
                      </Button>
                      <Button onPress={() => this.toggleView('Actu')}>
                        <Text>Actu</Text>
                      </Button>
                      <Button onPress={() => this.toggleView('contact')}>
                        <Text>Nous contacter</Text>
                      </Button>
                  </FooterTab>
              </Footer>
          </Container>
        );
    }
}

export default Home;
