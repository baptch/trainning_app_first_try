import { Text, View, Image, ScrollView, Linking} from 'react-native';
import { Card, Button } from 'react-native-elements';
import React from 'react';

class Contact extends React.Component {
    constructor(props){
        super(props);
    }

    static navigationOptions = {
        title: "Nous contacter"
    };


    render() {
        return (
          <View>
              <Text>Vous pouvez nous contacter par mail à toto@gmail.com</Text>
          </View>
        );
    }
}

export default Contact;
