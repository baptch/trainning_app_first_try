import { Text, View} from 'react-native';
import React from 'react';
import Stage from '../components/Stage';

class Stages extends React.Component {
    constructor(props){
        super(props);
    }

    static navigationOptions = {
        title: "Les prochains Stages"
    };

    render() {
        return (
          <View>

            <Text>Prochainement les stages</Text>
            <Stage/>
          </View>
        );
    }
}

export default Stages;
