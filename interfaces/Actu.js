import { Text, View, Image, ScrollView, Linking} from 'react-native';
import { Card, Button } from 'react-native-elements';
import React from 'react';
// import json from '../programme/test.json';

class Actu extends React.Component {
    constructor(props){
        super(props);
        this.state = {
          articles: []
        }
    }

    static navigationOptions = {
        title: "L'actualité du Krav"
    };

    componentDidMount(){
      fetch('http://164.132.48.157:5000/articles')
        .then(response => response.json())
        .then(articles => this.setState({ articles }));
        // .catch((error) => {
        //   console.error(error);
        // });
    }

    //mettre une key id sur le card
    render() {
        const { articles } = this.state;
        return (
          <View>
            <ScrollView>
            {articles.map((article, index) => (
                <Card key={index} title={article.title}>
                  <Text>Ceci est le texte de l'article: {article.text}</Text>
                  <Image
                    source={{ uri: article.url_image }}
                    style={{ width: 200, height: 200 }}
                  />
                  <Button onPress={() => Linking.openURL(article.source_url)} title="Allez à l'article"/>
                </Card>
            ))}
            </ScrollView>
          </View>
        );
    }
}

export default Actu;
