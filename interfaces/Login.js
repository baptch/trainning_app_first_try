import React, { Component } from 'react'
import { Text, View} from 'react-native';
import { Button } from 'react-native-elements';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

class Login extends Component {
  constructor(props) {
    super(props)
    this.onSignIn = this.onSignIn.bind(this);
  }

  onSignIn(){
      this.props.navigation.navigate('Home')
  }

  static navigationOptions = {
      title: "Login"
  };

  render() {
    return (
      <View>
        <Input placeholder='User name' shake={true}
        leftIcon={
          <Icon
            name='user'
            size={24}
            color='black'
          />
        }
      />

      <Input secureTextEntry={true} placeholder='password' shake={true}
      leftIcon={
        <Icon
          name='chevron-left'
          size={24}
          color='black'
        />
        }
      />
      <Button title="validate" onPress={this.onSignIn}/>
      </View>
    );
  }
}

export default Login;
