import React from 'react';
import { StyleSheet, View } from 'react-native';
// import { Button } from 'react-native-elements';
import { Button } from 'react-native';
// import { Button, Text } from 'native-base';
import CustomTableView from '../components/CustomTableView';
import Stages from './Stages';
import Actu from './Actu';
import Contact from './Contact';

class Presentation extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
              <View style={styles.buttonPlacement}>
                <Button onPress={() => this.props.navigation.navigate('Stages')} title="Stages" style={styles.buttonSpace}/>
                <Button onPress={() => this.props.navigation.navigate('CustomTableView')} title="Programmes" style={styles.buttonSpace}/>
                <Button onPress={() => this.props.navigation.navigate('Actu')} title="Actu"/>
                <Button onPress={() => this.props.navigation.navigate('Contact')} title="Contact"/>
              </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  buttonPlacement: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    marginTop:20,
    alignItems: 'center',
  },
  buttonSpace: {
    marginTop:30,
    paddingTop:30
  }
});

export default Presentation;
