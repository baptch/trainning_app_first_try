import { ListItem } from 'react-native-elements'
import { Text, View} from 'react-native';
import React from 'react';
import Programme from './Programme';

class CustomTableView extends React.Component {
    constructor(props){
        super(props);
        this.BeltsList = [
          {
            ceinture: 'Jaune',
            url: 'https://media.istockphoto.com/vectors/martial-art-belt-icon-vector-id934158158',
          },
          {
            ceinture: 'Orange',
            url: 'https://media.istockphoto.com/vectors/martial-art-belt-icon-vector-id934158158',
          },
          {
            ceinture: 'Verte',
            url: 'https://media.istockphoto.com/vectors/martial-art-belt-icon-vector-id934158158',
          }
        ]
    }
    static navigationOptions = {
        title: "Programme des ceintures"
    };

    render() {
        return (
          <View>
          {
            this.BeltsList.map((l, i) => (
              <ListItem
                key={i}
                leftAvatar={{ source: { uri: l.url } }}
                title={l.ceinture}
                onPress={() => this.props.navigation.navigate('Programme')}
              />
            ))
          }
          </View>
        );
    }
}
// Créer des fonctions qui vont appeler être les même que celles dans actions sur chaque items de la liste
// Il va falloir faire quelque chose dans ce style this.props.changeColorYellow pour tranmettre la couleur yellow
// La fonction changeColorYellow devra être implémenter dans la class, réutiliser ou caller dans le dispatche
// DANS LA FONCITON DE LA CLASSE on va pouvoir rajouter un changement de vue et ainsi afficher le programme
export default CustomTableView;
