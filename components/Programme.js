import { Text, View, StyleSheet} from 'react-native';
import React from 'react';
import { Card, Button, Icon } from 'react-native-elements'
import json from '../programme/test.json';


// console.log('c est ma liste technique', json.UV1.list_technique)
class Programme extends React.Component {
    constructor(props){
        super(props);
    }

    static navigationOptions = {
        title: "Programme"
    };

    render() {

        return (
          <View>
            {
              json.map((value, index) => (
                <View key={index}>
                  <Text style={styles.uv_level} >UV{index+1}: {value.uv_name}</Text>
                  <Text style={styles.tech_name}>Technique: {value.list_technique.map(
                    (valua, indexing)=> valua.name)}</Text>
                  <Text style={styles.tech_description}>Description: {value.list_technique.map(
                    (valua, indexing)=> valua.description)}</Text>
                </View>
              ))
            }
          </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  uv_level: {
    marginTop: 20,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  tech_name: {
    marginTop: 20,
    fontSize: 14,
    textAlign: 'left'
  },
  tech_description: {
    marginTop: 10,
    fontSize: 14,
    textAlign: 'left'
  },
});


export default Programme;
