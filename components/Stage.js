import * as AddCalendarEvent from 'react-native-add-calendar-event';
import {PermissionsAndroid} from 'react-native';
import { Text, View, ScrollView} from 'react-native';
import React from 'react';
import { Card, Button, Icon } from 'react-native-elements'
import { Permissions, Calendar } from 'expo';

class Stage extends React.Component {
    constructor(props){
        super(props);
        this.onPress = this.onPress.bind(this);
        this.state = {
          stages: []
        }
        // this.state = {eventConfig: {title: "this is a title", startDate: '2019-03-05T09:08:00.000Z', endDate:'2019-03-05T11:08:00.000Z'}}
    }

    static navigationOptions = {
        title: "Stage"
    };

    async getPermissionExpo(){
      // const { calendarPermission } = permissionsCallResult;
      // const cameraPermission = permissionsCallResult.cameraPermission;
      const { status, expires, permissions } = await Permissions.askAsync(Permissions.CALENDAR);
      if (status !== 'granted') {
        alert('Hey! You might want to enable notifications for my app, they are good.');
      }
    }

    componentDidMount(){
      // this.getPermission()
      fetch('http://164.132.48.157:5000/stages')
        .then(response => response.json())
        .then(stages => this.setState({ stages }));
      this.getPermissionExpo()
    }

    // async getPermission(){
    //   const granted = await PermissionsAndroid.request(
    //     PermissionsAndroid.PERMISSIONS.WRITE_CALENDAR,
    //     {
    //       title: 'Cool App use Calendar Permission',
    //       message:
    //         'We would use calendar permission to add event',
    //       buttonNeutral: 'Ask Me Later',
    //       buttonNegative: 'Cancel',
    //       buttonPositive: 'OK',
    //     },
    //   );
    //   if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    //     console.log('You can use the calendar');
    //   } else {
    //     console.log('calendar permission denied');
    //   }
    // }

    // requestCameraPermission() {
    //   try {
    //     const granted = PermissionsAndroid.request(
    //       PermissionsAndroid.PERMISSIONS.WRITE_CALENDAR,
    //       {
    //         title: 'Cool App use Calendar Permission',
    //         message:
    //           'We would use calendar permission to add event',
    //         buttonNeutral: 'Ask Me Later',
    //         buttonNegative: 'Cancel',
    //         buttonPositive: 'OK',
    //       },
    //     );
    //     if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    //       console.log('You can use the camera');
    //     } else {
    //       console.log('Camera permission denied');
    //     }
    //   } catch (err) {
    //     console.warn(err);
    //   }
    // }

    // onPress(){
    //   // this.requestCameraPermission()
    //   AddCalendarEvent.presentEventCreatingDialog(this.state.eventConfig)
    //   .then((eventInfo: { calendarItemIdentifier: string, eventIdentifier: string }) => {
    //       // handle success - receives an object with `calendarItemIdentifier` and `eventIdentifier` keys, both of type string.
    //       // These are two different identifiers on iOS.
    //       // On Android, where they are both equal and represent the event id, also strings.
    //       // when { action: 'CANCELED' } is returned, the dialog was dismissed
    //       console.warn(JSON.stringify(eventInfo));
    //     })
    //     .catch((error: string) => {
    //       console.warn(error);
    //   });
    // }

    onPress(){
      let details = {
        title: 'MyStage',
        // startDate: new Date("2019-03-12").toISOString(),
        startDate: new Date("March 12, 2019 13:30:00"),
        endDate: new Date("March 12, 2019 14:30:00"),
        timeZone: "Europe/Paris",
        notes: 'Coucou'
      }
      console.log(new Date("March 12, 2019 13:30:00"))
      Calendar.createEventAsync('Expo.Calendar.DEFAULT', details).then( event => {
        console.log(event);
      })
      .catch( error => {
        console.log(error);
      });
    }

    render() {
        const { stages } = this.state;
        return (
            <View>
              <ScrollView>
                {stages.map((stage, index) => (
                  <Card key={index}
                  title='Stage pour'>
                  <Text style={{marginBottom: 10}}>La Date: {stage.event_date}
                  </Text>
                  <Text style={{marginBottom: 10}}>Le Lieu: {stage.lieu}
                  </Text>
                  <Text style={{marginBottom: 10}}>Horaire:
                  </Text>
                  <Text style={{marginBottom: 10}}>Le Prix: {stage.prix}
                  </Text>
                  <Text style={{marginBottom: 10}}>Instructeur(s): {stage.formateur}
                  </Text>
                  <Button
                    backgroundColor='#03A9F4'
                    buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
                    title='Enregistrer la date dans mon agenda'
                    onPress={this.onPress}/>
                  </Card>
              ))}
          </ScrollView>
        </View>
        );
    }
}

export default Stage;
