import React from 'react';
import { StyleSheet, View } from 'react-native';
import AppStackNavigator from './navigation/AppStackNavigator';
import {createAppContainer } from 'react-navigation';
import { Text, Image } from 'react-native-elements';
import { Root } from 'native-base';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import belt from './redux';
import { createStore, applyMiddleware, compose } from 'redux';

const store = createStore(belt, compose(applyMiddleware(thunk)));
//essayer en kickan les commentaire ou en elevant la police
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fontsLoaded: false,
    }
    this.image = "http://fr.web.img5.acsta.net/r_1280_720/newsv7/18/01/12/14/34/40225780.jpg"
    this.loadFonts();
  }
  async loadFonts() {
    try {
      await Expo.Font.loadAsync({
        'Roboto': require('./node_modules/native-base/Fonts/Roboto.ttf'),
        'Roboto_medium': require('./node_modules/native-base/Fonts/Roboto_medium.ttf'),
      });
        this.setState({ fontsLoaded: true });
    }
    catch (err) {
      console.log(err);
    }
  }
  render() {

    const AppContainer = createAppContainer(AppStackNavigator);
    const fontsLoaded = this.state.fontsLoaded;
    if (fontsLoaded)
      return (
        // <Provider store={store}>
          <Root>
            <AppContainer />
          </Root>
        // </Provider>
      );
    else
      return <View><Image source={{ uri: this.image }} /></View>
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
