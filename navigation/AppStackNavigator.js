import React from 'react'
import { createStackNavigator } from 'react-navigation';
import Login from '../interfaces/Login';
import Home from '../interfaces/Home';
import Stages from '../interfaces/Stages';
import Actu from '../interfaces/Actu';
import CustomTableView from '../components/CustomTableView';
import Programme from '../components/Programme';
import Contact from '../interfaces/Contact';

const AppStackNavigator = createStackNavigator({
    Login,
    Home,
    CustomTableView,
    Stages,
    Actu,
    Contact,
    Programme
}, {
    initialRouteName: 'Home'
});
export default AppStackNavigator;
