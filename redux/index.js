import { combineReducers } from 'redux';
import belt from './belt';

export default combineReducers({
  belt,
});
