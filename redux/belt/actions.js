export const BELT_COLOR_YELLOW = 'BELT_COLOR_YELLOW';
export const BELT_COLOR_GREEN = 'BELT_COLOR_GREEN';
export const BELT_COLOR_ORANGE = 'BELT_COLOR_ORANGE';

export const changeColorYellow = (color) => {
  return {
    type: BELT_COLOR_YELLOW,
    color,
  }
}

export const changeColorOrange = (color) => {
  return {
    type: BELT_COLOR_ORANGE,
    color,
  }
}

export const changeColorGreen = (color) => {
  return {
    type: BELT_COLOR_GREEN,
    color,
  }
}
