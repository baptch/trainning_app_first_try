import { BELT_COLOR_YELLOW } from './actions';
import { BELT_COLOR_ORANGE } from './actions';
import { BELT_COLOR_GREEN } from './actions';

const initialState = 'yellow';

export default (state = initialState, action) => {
  switch (action.type) {
    case BELT_COLOR_YELLOW:
      state = 'yellow';
      return state;
    case BELT_COLOR_ORANGE:
      state = 'orange';
      return state;
    case BELT_COLOR_GREEN:
      state = 'green';
      return state;
    default:
      return state;
  }
}
